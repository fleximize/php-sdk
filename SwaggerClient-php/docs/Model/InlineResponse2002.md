# InlineResponse2002

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**application_status** | [****](.md) |  | [optional] 
**internal_score_status** | **string** | String describing if application passed, failed or have been referred for manual review | [optional] 
**internal_id** | **string** | Client internall ID | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

